// Id contains functions for generating random ids.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package rand

import (
	"fmt"
	"math/rand"
	"time"
)

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// ID8 generates a random 8 byte ID string. This is not intended for cryptographic use.
func ID8() string {
	now := time.Now()
	s1 := fmt.Sprintf("%x", uint16(now.Unix()))
	switch len(s1) {
	case 0:
		s1 = "0000"
	case 1:
		s1 = "000" + s1
	case 2:
		s1 = "00" + s1
	case 3:
		s1 = "0" + s1
	}
	s2 := fmt.Sprintf("%x", uint8(rand.Int31()))
	switch len(s2) {
	case 0:
		s2 = "00"
	case 1:
		s2 = "0" + s2
	}
	s3 := fmt.Sprintf("%x", uint8(now.UnixNano()))
	switch len(s3) {
	case 0:
		s3 = "00"
	case 1:
		s3 = "0" + s3
	}
	return s1 + s2 + s3
}
