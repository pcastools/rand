// Random wraps crypto/rand and provides utility function for working with random numbers

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package rand

import (
	"crypto/rand"
	"encoding/binary"
	"time"
)

//////////////////////////////////////////////////////////////////////
// Public functions
//////////////////////////////////////////////////////////////////////

// Byteslice populates the slice b with uniformly-distributed random bytes.  The initial call may block until there is enough entropy in the system; subsequent calls will not block.
func Byteslice(b []byte) {
	// read from cryto/rand until we can read enough bytes
	for {
		if _, err := rand.Read(b); err == nil {
			return
		}
		<-time.After(500 * time.Millisecond)
	}
}

// Byte returns a uniformly-distributed random byte.  The initial call may block until there is enough entropy in the system; subsequent calls will not block.
func Byte() byte {
	b := make([]byte, 1)
	Byteslice(b)
	return b[0]
}

// Int64 returns a uniformly-distributed random int64.  The initial call may block until there is enough entropy in the system; subsequent calls will not block.
func Int64() int64 {
	return int64(Uint64())
}

// Uint64 returns a uniformly-distributed random uint64.  The initial call may block until there is enough entropy in the system; subsequent calls will not block.
func Uint64() uint64 {
	b := make([]byte, 8)
	Byteslice(b)
	return binary.BigEndian.Uint64(b)
}

// Int32 returns a uniformly-distributed random int32.  The initial call may block until there is enough entropy in the system; subsequent calls will not block.
func Int32() int32 {
	return int32(Uint32())
}

// Uint32 returns a uniformly-distributed random uint32.  The initial call may block until there is enough entropy in the system; subsequent calls will not block.
func Uint32() uint32 {
	b := make([]byte, 4)
	Byteslice(b)
	return binary.BigEndian.Uint32(b)
}

// Int16 returns a uniformly-distributed random int16.  The initial call may block until there is enough entropy in the system; subsequent calls will not block.
func Int16() int16 {
	return int16(Uint16())
}

// Uint16 returns a uniformly-distributed random uint16.  The initial call may block until there is enough entropy in the system; subsequent calls will not block.
func Uint16() uint16 {
	b := make([]byte, 2)
	Byteslice(b)
	return binary.BigEndian.Uint16(b)
}

// Int8 returns a uniformly-distributed random int8.  The initial call may block until there is enough entropy in the system; subsequent calls will not block.
func Int8() int8 {
	return int8(Byte())
}

// Uint8 returns a uniformly-distributed random uint8.  The initial call may block until there is enough entropy in the system; subsequent calls will not block.
func Uint8() uint8 {
	return uint8(Byte())
}
